package io.viberzmusic

import io.ktor.server.application.*
import io.viberzmusic.plugins.*

fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)
}

fun Application.module() {
    configureSerialization()
    configureRouting()
}
