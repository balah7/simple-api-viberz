package io.viberzmusic.models

import kotlinx.serialization.Serializable
import java.time.LocalDateTime
import java.util.UUID

@Serializable
data class Music(
    val title: String,
    val artist: String,
    val minutes: Double,
    val feets: List<String>? = null,
    val album: String = title,
) {
    val id: String = UUID.randomUUID().toString()
    val createdAt: String = LocalDateTime.now().toString()
}