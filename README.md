# ViberzMusic API

## Overview

This API provides endpoints to manage music data.

## Endpoints

### GET /

- Description: Welcome message.
- Response:
    - Status Code: 200 OK
    - Body: "Welcome to my API. Use the /music route!"

### GET /musics

- Description: Get all music data.
- Response:
    - Status Code: 200 OK
    - Body: JSON array of music objects

### GET /music/{id}

- Description: Get music data by ID.
- Parameters:
    - id: ID of the music
- Response:
    - Status Code: 200 OK
    - Body: JSON representation of the music object
    - Status Code: 404 Not Found
    - Body: "Music was not found" if the music with the given ID does not exist

### POST /music

- Description: Create a new music entry.
- Request Body: JSON representation of the music object
- Response:
    - Status Code: 201 Created
    - Body: "Music was created" if the creation was successful
    - Status Code: 501 Not Implemented
    - Body: "It was not possible to create a new music" if the creation failed

### DELETE /music/{id}

- Description: Delete a music entry by ID.
- Parameters:
    - id: ID of the music to delete
- Response:
    - Status Code: 200 OK
    - Body: "Music was deleted" if the deletion was successful
    - Status Code: 404 Not Found
    - Body: "Music was not found" if the music with the given ID does not exist
    - Status Code: 304 Not Modified
    - Body: "Unable to delete the music" if the deletion failed

## Error Handling

- The API returns appropriate status codes and error messages for failed operations.

## Technologies Used

- Kotlin
- Ktor
- Kotlinx Serialization
