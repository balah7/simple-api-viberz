package io.viberzmusic.repository

import io.viberzmusic.models.Music

class MusicRepository {

    val musics get() = _musics.toList()

    fun save(music: Music) {
        _musics.add(music)
    }

    fun delete(id: String?): Boolean {
        if(!id.isNullOrEmpty()) {
            val music = _musics.find { it.id == id }
            _musics.remove(music)
            return music != null
        }
        return false
    }

    companion object {
        private val _musics = mutableListOf<Music>()
    }
}