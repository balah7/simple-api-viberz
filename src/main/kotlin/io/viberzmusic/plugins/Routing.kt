package io.viberzmusic.plugins

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.viberzmusic.models.Music
import io.viberzmusic.repository.MusicRepository
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

fun Application.configureRouting() {
    val repository = MusicRepository()

    routing {
        get("/") {
            call.respondText("Welcome to my API. Use the /music route!", status = HttpStatusCode.OK)
        }

        get("/musics") {
            call.respond(repository.musics)
        }

        get("/music/{id}") {
            val id = call.parameters.get("id")
            val music = repository.musics.find { it.id == id }
            if (music is Music) {
                call.respondText(
                    Json.encodeToString(music),
                    contentType = ContentType.Application.Json,
                    status = HttpStatusCode.OK
                )
            } else {
                call.respondText("Music was not found", status = HttpStatusCode.NotFound)
            }
        }

        post("/music") {
            val execution = runCatching {
                val music = call.receive<Music>()
                repository.save(music)
            }

            execution.onSuccess {
                call.respondText("Music was created", status = HttpStatusCode.Created)
            }

            execution.onFailure {
                call.respondText("It was not possible to create a new music", status = HttpStatusCode.NotImplemented)
                println(it.message)
            }
        }

        delete("/music/{id}") {
            val id = call.parameters.get("id")
            var found: Boolean? = null
            val execution = runCatching {
                found = repository.delete(id)
            }

            execution.onSuccess {
                if (found == true) {
                    call.respondText("Music was deleted", status = HttpStatusCode.OK)
                } else {
                    call.respondText("Music was not found", status = HttpStatusCode.NotFound)
                }
            }

            execution.onFailure {
                call.respondText("Unable to delete the music", status = HttpStatusCode.NotModified)
                println(it.message)
            }
        }
    }
}
